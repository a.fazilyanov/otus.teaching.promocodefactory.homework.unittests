﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnNotFoundResult()
        {
            // Arrange
            var partner = new PartnerBuilder().Simple().Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => null);

            // Act
            var act = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest());

            // Assert
            act.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_BadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder().Simple().Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => new Partner { IsActive = false });

            // Act
            var act = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest());

            // Assert
            act.Should().BeAssignableTo<BadRequestObjectResult>();
        }



        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithActiveLimit_NumberIssuedPromoCodesChangedToZero()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .Simple()
                .IsActive()
                .WithNumberIssuedPromoCodes(73)
                .WithLimit()
                .Build();

            var request = new LimitRequestBuilder()
                .Simple()
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithNotActiveLimit_NumberIssuedPromoCodesNonChanged()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .Simple()
                .IsActive()
                .WithNumberIssuedPromoCodes(73)
                .Build();

            var request = new LimitRequestBuilder()
                .Simple()
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(73);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithActiveLimit_ActiveLimitHaveCancelDate()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .Simple()
                .IsActive()
                .WithNumberIssuedPromoCodes(73)
                .WithLimit()
                .Build();

            var request = new LimitRequestBuilder()
                .Simple()
                .Build();

            var activeLimit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            activeLimit
                .CancelDate
                .Should()
                .HaveValue();
        }


        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitLessOrEqualZero_BadRequest(int count)
        {

            // Arrange
            var partner = new PartnerBuilder()
                .Simple()
                .IsActive()
                .WithNumberIssuedPromoCodes(73)
                .WithLimit()
                .Build();

            var request = new LimitRequestBuilder()
                .Simple()
                .WithLimit(count)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            // Act
            var act = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            act.Should().BeAssignableTo<BadRequestObjectResult>();
        }



        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_AddLimit_LimitSavedToDb()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .Simple()
                .IsActive()
                .WithNumberIssuedPromoCodes(73)
                .WithLimit()
                .Build();

            var request = new LimitRequestBuilder()
                .Simple()
                .WithLimit(73)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }




    }
}