﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class LimitRequestBuilder
    {
        private SetPartnerPromoCodeLimitRequest _request;

        public LimitRequestBuilder()
        {

        }

        public LimitRequestBuilder Simple()
        {
            _request = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.MaxValue,
                Limit = 73
            };
            return this;
        }


        public LimitRequestBuilder WithLimit(int count)
        {
            _request.Limit = count;
            return this;
        }

        public LimitRequestBuilder WithEndDate(DateTime date)
        {
            _request.EndDate = date;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }
    }

}