﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class PartnerBuilder
    {
        private Partner _partner;

        public PartnerBuilder()
        {

        }

        public PartnerBuilder Simple()
        {
            _partner = new Partner
            {
                Id = Guid.NewGuid(),
                NumberIssuedPromoCodes = 0,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            return this;
        }

        public PartnerBuilder IsActive()
        {
            _partner.IsActive = true;
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int count)
        {
            _partner.NumberIssuedPromoCodes = count;
            return this;
        }

        public PartnerBuilder WithLimit()
        {
            _partner.PartnerLimits.Add(new PartnerPromoCodeLimit
            {
                Id = Guid.NewGuid(),
                Limit = 73
            });
            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }

}